``DataExchangeProxy`` Interface
===============================

An instance of the ``DataExchangeProxy`` class is accessible via the ``data`` property of the derived
:doc:`SegmentationAlgorithm <./segmentation_algorithm_interface>` instance. It is used to communicate with the core
module to exchange data.


Methods
-------

Methods of ``DataExchangeProxy``.


``getFrameCount()``
^^^^^^^^^^^^^^^^^^^

    Signature:
        int ``getFrameCount`` ()
    Return:
        int
    Description:
        Returns the total number of frames for the current motion recording.


``getMMMFile()``
^^^^^^^^^^^^^^^^

    Signature:
        String ``getMMMFile`` ()
    Return:
        String
    Description:
        Returns the `MMM <https://mmm.humanoids.kit.edu/index.html>`_ file of the current motion recording as String.
        MMM is an XML format file which can be parsed with various XML readers or the already existing
        `MotionReaderXML <https://mmm.humanoids.kit.edu/classMMM_1_1MotionReaderXML.html>`_ for C++.


``getGroundTruth()``
^^^^^^^^^^^^^^^^^^^^

    Signature:
        List<int> ``getGroundTruth`` ()
    Return:
        List<int>
    Description:
        Returns the ground truth for the current motion recording with the granularity determined by the
        ``trainingGranularity`` property of the derived
        `SegmentationAlgorithm <./segmentation_algorithm_interface>`_ instance.


``getJointAnglesForFrame()``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    Signature:
        List<float> ``getJointAnglesForFrame`` (int ``frameNumber`` )
    Return:
        List<float>
    Description:
        Returns the joint angles of the frame with the number ``frameNumber`` for the current motion recording.


``reportKeyFrame()``
^^^^^^^^^^^^^^^^^^^^

    Signature:
        void ``reportKeyFrame`` (int ``frameNumber`` )
    Return:
        void
    Description:
        Reports that the frame with the number ``frameNumber`` is a key frame where a segmentation occurs.


``reportKeyFrames()``
^^^^^^^^^^^^^^^^^^^^^

    Signature:
        void ``reportKeyFrames`` (List<int> ``frameNumbers`` )
    Return:
        void
    Description:
        Reports that the list of frame numbers ``frameNumbers`` are key frames where segmentations occur.
