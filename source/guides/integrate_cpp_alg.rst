Integrate a C++ Algorithm
=========================

This guide will show you how to setup your workspace to integrate your own C++
motion segmentation algorithm using the C++ PLI. It is assumed that you either
followed the :doc:`quickstart guide <./quickstart>` or you
:doc:`compiled MSeg from source <../advanced_guides/compile_mseg>`.


Install the Skeleton Project
----------------------------

We will use the ``mseggen`` terminal tool to install a skeleton project.

.. code-block:: bash

    # Define the base directory where the project directory should be located
    $ export $PROJECT_BASE_DIR=~/projects
    # Run the generation tool to create the skeleton.
    # If your algorithm requires training, add the --train flag
    $ mseggen -b $PROJECT_BASE_DIR --cpp ExampleAlgorithm


Compiling the Algorithm
-----------------------

Your project should now look similar to this (If you changed the algorithm name
the paths will be slightly altered):

.. code-block:: bash

    $PROJECT_BASE_DIR/
    `--- examplealgorithm
        |--- build/
        |--- source/
        |    |--- examplealgorithm.cpp
        |    |--- examplealgorithm.h
        |    `--- main.cpp
        `--- CMakeLists.txt

To compile the skeleton, we change into the build folder and run the
corresponding commands like this:

.. code-block:: bash

    # Change into the build folder
    $ cd $PROJECT_BASE_DIR/examplealgorithm/build
    # Run CMake
    $ cmake ..
    # Run make
    $ make

You may get warnings about unused parameters, but eventually, the project should
compile.


Run the Algorithm
-----------------

To run the algorithm, both ArmarX and the MSeg core module must be running. You
can then start the algorithm like this:

.. code-block:: bash

    # Change into the build folder
    $ cd $PROJECT_BASE_DIR/examplealgorithm/build
    # Run the algorithm
    $ ./examplealgorithm

You should now see an output like this:

.. code-block:: bash

    user@machine:~/projects/examplealgorithm/build$ ./examplealgorithm
    Connecting to MSeg core module...
    Connection established
    ExampleAlgorithm ready

You can stop the algorithm with :kbd:`Ctrl` + :kbd:`C` at any time.


How to Proceed
--------------

You can now start implementing your algorithm. You may also want to get more
familiar with the
:doc:`SegmentationAlgorithm API reference <../reference/segmentation_algorithm_interface>`,
the :doc:`MSeg GUI <../reference/mseg_gui>` and the
:doc:`msegcm, msegdata, and mseggen terminal tools<../reference/mseg_terminal_tools>`.
