Quickstart
==========

This guide will show how to install MSeg on a 64 bit Ubuntu machine. This includes the MSeg Core
Module, all PLIs, and all available tools to use with MSeg.

.. note:: It is *strongly recommended* to use MSeg with Ubuntu 14.04 x64, as it eases the
    installation process drastically. If this should be a problem, consider using a virtual
    machine like `VirtualBox <https://www.virtualbox.org/>`_ and install Ubuntu 14.04 there.


Preconditions
-------------

- Freshly installed `Ubuntu 14.04.5 LTS x64 trusty <http://releases.ubuntu.com/14.04/>`_
  [64-bit PC (AMD64) desktop image]
- ``sudo`` privileges
- Approx. hard disk size required: 1 GiB (Make sure to have enough space if you want to
  install additional software like IDE's or MATLAB)


Installation
------------

Open a terminal ( :kbd:`Ctrl` + :kbd:`Shift` + :kbd:`T` ) and add the H2T
package server which includes all neccessary binaries.

.. code-block:: bash

    # Add H2T signing key and package server
    $ curl https://packages.humanoids.kit.edu/h2t-key.pub | sudo apt-key add -
    $ echo -e "deb http://packages.humanoids.kit.edu/trusty/main trusty main\ndeb http://packages.humanoids.kit.edu/trusty/testing trusty testing" | sudo tee /etc/apt/sources.list.d/armarx.list
    # Update lists
    $ sudo apt update

Now MSeg can be installed. This command will download roughly 300 MiB, resulting in
about 1 GiB after installation.

.. code-block:: bash

    # Download and install MSeg and all its dependencies
    $ sudo apt install mseg

Depending on what programming language(s) you prefer for your algorithm, you should
also install the corresponding Programming Language Interface(s):

.. code-block:: bash

    # For C++ algorithms
    $ sudo apt install mseg-pli-cpp
    # For Java algorithms
    $ sudo apt install mseg-pli-java
    # For Python algorithms
    $ sudo apt install mseg-pli-python
    # For MATLAB algorithms
    $ sudo apt install mseg-pli-matlab


Initialisation
--------------

After the installation, both ArmarX and MSeg need to be initialised.

.. code-block:: bash

    # Run armarx for the first time. You will be prompted to add a line to
    # .bashrc for autocompletion. Accept with `y`
    $ armarx
    # Source .bashrc as indicated
    $ source ~/.bashrc
    # Download the MSeg datasets of labelled whole-body motion recordings
    $ msegdata fetch

Now we're ready for the first run.

.. code-block:: bash

    # First, we need to start ArmarX
    $ armarx start
    # Now we need to start the MSeg core module
    $ msegcm start
    # Eventually, launch the ArmarX GUI
    $ armarx gui


The GUI will now load and eventualy, a popup should appear.

- You may check the checkbox :kbd:`Do not show again`.
- Push the :kbd:`Open empty GUI` button.
- You can now use the MSeg GUI by clicking on :kbd:`Add Widget | MSeg`.

The MSeg GUI should now show up like this:

.. image:: ../_static/images/step-6-deps-resolved.png


How to Proceed
--------------

With MSeg being set up, you can proceed with integrating your own motion segmentation algorithm in
:doc:`C++ <./integrate_cpp_alg>`, :doc:`Java <./integrate_java_alg>`, :doc:`Python <./integrate_python_alg>`
or :doc:`MATLAB <./integrate_matlab_alg>`.
