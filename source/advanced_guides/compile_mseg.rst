Compile MSeg from Source
========================

This guide will show how to compile the MSeg core module from source on a 64 bit Ubuntu machine.

.. note:: It is *strongly recommended* to use the MSeg core module with Ubuntu 14.04, as it eases the installation
    process drastically. If this should be a problem, consider using a virtual machine like
    `VirtualBox <https://www.virtualbox.org/>`_ and install Ubuntu 14.04 there.


.. _ArmarXCore Ubuntu dependencies: https://armarx.humanoids.kit.edu/ArmarXDoc-Installation.html#ArmarXDoc-Dependecies-Installation-standard-deb
.. _H2T package server: https://armarx.humanoids.kit.edu/ArmarXDoc-Installation-deb.html#ArmarXDoc-Installation-Setup-deb
.. _ArmarX installation from source guide: https://armarx.humanoids.kit.edu/ArmarXDoc-ArmarX-Installation-Manual.html
.. _ArmarX repositories: https://gitlab.com/armarx
.. _MSeg repositories: https://gitlab.com/h2t/kit-mseg


Preconditions
-------------

- Freshly installed `Ubuntu 14.04.5 LTS x64 "trusty" <http://releases.ubuntu.com/14.04/>`_
  [64-bit PC (AMD64) desktop image]
- ``sudo`` privileges
- Approx. hard disk size required: 1 GiB (Make sure to have enough space if you want to install additional software
  like IDEs or MATLAB)


The H2T Package Server
----------------------

First, we will add the H2T package server so that the packages MSeg depends on become available. Open a terminal
( :kbd:`Ctrl` + :kbd:`Shift` + :kbd:`T` ) and run:

.. code-block:: bash

    # Add H2T key and add repositories
    $ curl https://packages.humanoids.kit.edu/h2t-key.pub | sudo apt-key add -
    $ echo -e "deb http://packages.humanoids.kit.edu/trusty/main trusty main\ndeb http://packages.humanoids.kit.edu/trusty/testing trusty testing" | sudo tee /etc/apt/sources.list.d/armarx.list
    # Update lists
    $ sudo apt update


The MSeg Umbrella Repository
----------------------------

Due to the variety of programming languages, MSeg is split into several repositories. To ease the installation
and setup, we provide the MSeg Umbrella Repository which manages the dependencies between the subprojects.
To fetch the repository, we will need Git:

.. code-block:: bash

    # Install Git
    $ sudo apt install git

Now we can clone the MSeg Umbrella Repository:

.. code-block:: bash

    # Clone the MSeg Umbrella Repository
    $ git clone https://gitlab.com/h2t/kit-mseg/mseg.git
    # Change into the repository
    $ cd mseg

If you are a developer, you will probably want to leave the release untouched to use the latest versions
of all subprojects and directly skip to the next section.

However, for using MSeg productively or packaging it, a stable release must be checked out. The releases
of the MSeg Umbrella Repository are coupled with the MSeg Core Module. Each tagged release of the MSeg Core Module
corresponds with a branch in the umbrella repository.

.. code-block:: bash

    # Check out a stable release
    $ git checkout 1.2.0


Initialising MSeg
^^^^^^^^^^^^^^^^^

The MSeg Umbrella Repository comes with a makefile which exposes several rules. One of them will be used
to initialise all subprojects of MSeg, namely ``make init``.

The ``init`` rule is designed to fail as long as the environment is not set up properly for MSeg to run. This
includes cloning and using the right versions, dependency checking, and environment variable setup. When
the ``init`` rule fails, it will print the proper command to fix the situation. Do so, and run ``make init``
again afterwards.

The following setup excerpt will show each possible fail -- don't be confused if the ``init`` rule didn't fail
for you as depcited here -- this just means that the regarding requirement is already met and no action is needed.
Please read the actual error messages you get carefully.

.. code-block:: bash

    # Run init for the first time. This will take quite long
    # as the repositories will have to be downloaded.
    $ make init
    ... Cloning ...
    ... Checking out stable branches if set ...
    ERROR: Dependencies not satisfied. To install them, please run
    `make depsdeb | xargs sudo apt install -y`. This may download
    up to 1.1 GiB of data
    # This error means that there are Debian dependencies which are
    # missing. Running the suggested command will collect the required
    # Debian packages from all subprojects which can be piped to
    # apt
    $ make depsdeb | xargs sudo apt install -y
    ... Installing Debian dependencies ...
    # Since the last attempt failed, we need to run init again
    $ make init
    ...
    ERROR: Dependencies not satisfied. To install them, please run
    `make depspip | xargs sudo pip install`
    # This error occurs when there are Python dependencies which are
    # missing. Again, we run the suggested command
    $ make depspip | xargs sudo pip install
    ... Installing Python dependencies ...
    # Last init attempt failed, so retry
    $ make init
    ...
    ERROR: Dependencies not satisfied. To install them, please run
    `make depspip3 | xargs sudo pip3 install`
    # Similar to Python, the Python3 dependencies may be missing
    # and need to be installed as suggested
    $ make depspip3 | xargs sudo pip3 install
    ... Installing Python dependencies ...
    # Failed again, so re-run init
    $ make init
    ...
    ERROR: bashrc-file changed and needs to be resourced. Run
    `source ~/.bashrc`
    # This error occurs after the bashrc entries of all subprojects
    # were collected and changed or are not sourced at all. Simply
    # sourcing the ~/.bashrc should solve that
    $ source ~/.bashrc
    # init again
    $ make init
    Initialising: FINISHED SUCCESSFULLY
    # Now we're good to go


Installing MSeg
^^^^^^^^^^^^^^^

After we initialised MSeg successfully, we should now be able to safely compile and install MSeg.

.. code-block:: bash

    # Install MSeg
    $ make install

This rule will install all subprojects of MSeg so that the dependencies between the subprojects
are always met.

.. note::

    The install process may take quite a while, but you can utilise your multicore processor if
    you have one using the ``THREADS`` environment variable to speed it up. For example, if you
    have four cores, you could run ``THREADS=4 make install`` to make use of them. As of now,
    this only applies to C++ compilations, though.


Configuring ArmarX
------------------

With everything being set up, we can now start ArmarX for the first time. Config files needed
later will be created then.

.. code-block:: bash

    # Run armarx for the first time. You will be prompted to add a line to
    # .bashrc for autocompletion. Accept with y
    $ armarx
    # Source .bashrc as indicated
    $ source ~/.bashrc
    # Run armarx again since the previous step just modified the .bashrc file
    $ armarx start

The config file now got created. Run the following command and apply the changes below.

.. code-block:: bash

    # Open config file
    $ gedit ~/.armarx/default.cfg

Changes:

.. code-block:: diff

    ++ Ice.MessageSizeMax=10240
       Ice.Default.Locator=IceGrid/Locator:tcp -p 12454 -h localhost
       IceGrid.Registry.Client.Endpoints=tcp -p 12454

       ArmarX.MongoHost=localhost
       ArmarX.MongoPort=12455

       #Put your custom ArmarX Packages in this list, e.g. so that the gui can find their plugins.
    -- ArmarX.AdditionalPackages=
    ++ ArmarX.AdditionalPackages=MSeg


Save and close the config file. Finally, restart ArmarX.

.. code-block:: bash

    # Restart ArmarX
    $ armarx reset
    # Start ArmarX GUI
    $ armarx gui

The GUI will now load and eventualy, a popup should appear.

- You may check the checkbox :kbd:`Do not show again`.
- Push the :kbd:`Open empty GUI` button.


Starting the MSeg Core Module
-----------------------------

You can now use the MSeg GUI by clicking on :kbd:`Add Widget | MSeg`. You should see a
window similar to this:

.. image:: ../_static/images/step-1-no-deps.png

Note the red text which says:

    MSeg waiting for EvaluationController and SegmentationController

Both the ``EvaluationController`` and the ``SegmentationController``, as well as the ``DataExchange``,
which is not listed here, are components that form the core module. In order for the GUI to work,
these components have to be started.

.. code-block:: bash

    # Start the core module
    $ msegcm start

The components of the core module are now running. If you now head back to the MSeg GUI, you should
see that the warning showed before disappeared.

.. image:: ../_static/images/step-6-deps-resolved.png

Remember to start these components every time you reboot your computer. Also, when a component
crashes, it may be required to restart them using ``msegcm restart``. You will be notified if components
need to be restarted by the red text as shown in the first graphic.


How to Proceed
--------------

With MSeg being set up, you can proceed with integrating your own motion segmentation algorithm in
:doc:`C++ <../guides/integrate_cpp_alg>`, :doc:`Java <../guides/integrate_java_alg>`,
:doc:`Python <../guides/integrate_python_alg>` or :doc:`MATLAB <../guides/integrate_matlab_alg>`.
